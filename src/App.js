import React, { Component } from 'react';
import './App.css';
import {Note} from "./Note";
import {Editor} from "./Editor";
import {Category} from "./Category";


const getAllCategories = '/api/category';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: null,
            activeCategoryId: null,
            notes: null,
            activeNoteId: null,
            activeNote: null
        };
        this.handleCategoryClick = this.handleCategoryClick.bind(this);
        //this.handleAddCategory = this.handleAddCategory().bind(this);
    }

    loadCategories() {
        fetch(getAllCategories)
            .then(response => response.json())
            .then(loadedCategories => {
                this.setState( {
                    categories: loadedCategories
                })
            })
            .catch(error => console.log(error));
    }

    loadNotes(categoryId) {
        fetch('/api/' + categoryId)
            .then(response => response.json())
            .then(category => {
                this.setState( {
                    notes: category.notes
                })
            })
            .catch(error => console.log(error))
    }

    loadNote(noteId) {
        fetch('/api/' + this.state.activeCategoryId + '/' + noteId)
        .then(response => response.json())
            .then(note => {
                this.setState( {
                    activeNote: note
                })
            })
            .catch(error => console.log(error))
    }

    componentDidMount() {
        this.loadCategories();
    }

    handleCategoryClick(id) {
        this.setState({
            activeCategoryId: id
        });
        this.loadNotes(id);
    }

    handleNoteClick(id) {
        this.setState({
            activeNoteId: id
        });
        this.loadNote(id);
    }

    // handleAddCategory() {
    //     let ielem = document.getElementById('category-input');
    //         fetch(addCategoryQuery, {method: 'POST'})
    //             .then(response => response.json())
    //             .then(category => {
    //                 let updatedCategories = this.state.categories;
    //                 updatedCategories.push(category);
    //                 this.setState({categories: updatedCategories})
    //             })
    //             .catch(error => {
    //                 console.log(error)
    //             });
    //         ielem.value=' ';
    //     }

    renderCategories() {
        let categories = this.state.categories;
        if (categories == null) return;
        return categories.map( category => { return (
            <Category name={category.name} categoryId={category.id}
                      active={this.state.activeCategoryId === category.id}
                                            onClick={() => this.handleCategoryClick(category.id)}/>

        )})
    }

    renderNoteHeaders() {
        let notes = this.state.notes;
        if (notes == null) return;
        return notes.map(note => { return (
                <Note name={note.title} noteId={note.id} onClick={() => this.handleNoteClick(note.id)}
                active={this.state.activeNoteId === note.id}/>
        )})
    }


  render() {
    return (
        <div className="row">
            <div className='col-sm-3'>
                <ul>
                    {this.renderCategories()}
                </ul>
                <div className="input-group mb-3">
                <input id='category-input' type="text" className="form-control" placeholder="new category" aria-label="category name" aria-describedby="basic-addon2"/>
                <div className="input-group-append">
                    <button className="btn btn-outline-secondary" type="button">Add</button>
                </div>
            </div>

            </div>
            <div className='col-sm-3'>
                <ul>
                    {this.renderNoteHeaders()}
                </ul>
            </div>
            <div className='col-sm-6'>
                <Editor />
            </div>
        </div>
    );
  }
}

export default App;
