import React, {Component} from 'react'

export class Note extends Component {
    render() {
        return (
            <li key={this.props.noteId} onClick={this.props.onClick} className={this.props.active ? 'active' : ''}>
                {this.props.name}
            </li>
        )
    }
}