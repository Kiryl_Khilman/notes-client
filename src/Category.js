import React, {Component} from 'react'
import './Category.css'

export class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {active: false};
    }

    onClick() {
        this.setState({active: true});
}

    render() {
        return (
            <li key={this.props.categoryId} onClick={this.props.onClick} className={this.props.active ? 'active' : ''}>
            <span className='btn btn-secondary'>
            {this.props.name}
            </span>
            </li>
        )
    }
}