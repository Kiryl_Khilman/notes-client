import React, {Component} from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import './Note.css';

const modules = {
    toolbar: [
        [{'header': [1, 2, false]}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
        ['link', 'image'],
        ['clean']
    ],
};

const formats = [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image'
];

export class Editor extends Component {
    constructor(props) {
        super(props);
        this.state = {text: this.props.text};
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(value) {

        this.setState({text: value})
    }

    render() {
        return (
            <div id='editor-container'>
                <input type="text" className="form-control" id="note-title" placeholder="Enter title here.."/>
            <ReactQuill value={this.state.text} theme="snow" modules={modules} formats={formats}
                        onChange={this.handleChange} placeholder='Add new note here..'>
            </ReactQuill>
                <button className='btn btn-primary'>Save</button>
            </div>
        )
    }
}